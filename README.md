# Jenkinsin Java-tuen nostaminen JDK-version 11 ohi

OTP1-kurssin Jenkins-ohjetta seuranneille opiskelijoille seuraavan kaltainen virheviesti saattaa olla tässä vaiheessa tuttu (ja jos ei, se tulee melko varmasti tutuksi kun koontia yritetään ensimmäisen kerran):

![Ongelma](images/problem.png)

Tämä ongelma syntyy siitä, että kirjoitushetkellä lokakuussa 2021 Jenkins [tukee virallisesti yhä ainoastaan JDK 11:ta](https://www.jenkins.io/doc/administration/requirements/java/). Tämä saattaa olla ongelmallista, mikäli Java-projekti on luotu maaliskuun 2019 jälkeen, koska JDK 11 julkaistiin syyskuussa 2018 (ja sen aktiivinen kehitys loppui edellämainitusti maaliskuussa 2019).

JDK 11:ta seuranneet versiot sisältävät useita hyödyllisiä toimintoja - mm. paranneltu switch-lauseke (enhanced switch), moniviivaiset merkkijonoliteraalilausekkeet (multiline string literal) sekä Sealed- ja Record-luokkatyypit - joita ei ymmärrettävästi välttämättä haluta antaa pois vain sen takia, että Jenkins-koonti menisi läpi.

Tämä ohje kertoo, miten Jenkinsin JDK-tuki voidaan nostaa JDK 11:ta yli. Ohje olettaa, että Jenkins-palvelimen alkuasennus on suoritettu OTP1:n Jenkins-ohjeen pohjalta, Dockeriin ja `jenkins/jenkins:jdk11`-levynkuvaa käyttäen (tosin ohje on sama `lts-jdk11`-levynkuvalle kunnes toisin todistetaan). Ohjeen kuvat ovat myöskin macOS-käyttöjärjestelmästä, mutta harvojen alustariippuvaisten osioiden komennot ja niiden tulosteet ovat täysin samat Linuxilla.

> **Sivuseikka:** Jenkinsillä on myös `jdk17-preview`-levynkuva, mutta sen toimivuudesta ja/tai vakaudesta ei ole takeita - ei sen kummemmin ohjelmiston kehittäjien kuin allekirjoittaneenkaan osalta - ja tämä ohje on kirjoitettu sillä olettamuksella, että ongelma havaitaan vasta Jenkinsin konfiguraation viimeisessä vaiheessa eli koontia testatessa, jolloin koko työtä ei ymmärrettävästi enää välttämättä haluta heittää hiiteen ja aloittaa alusta (ottaen huomioon Jenkinsin tankean, takkuisen ja erittäin manuaalisen konfiguraatioprosessin).
>
> **Sivuseikka 2:** On yleisesti huono käytäntö peukaloida Docker-konttien sisältöä tällä tavalla, sillä jos kontin joutuu jossain vaiheessa tekemään uusiksi, katoaa uusien JDK:iden tuki sen mukana, ja tämä prosessi on tehtävä alusta uudelleen.
>
> Allekirjoittanut väittää kuitenkin, että koska missään vaiheessa ennen Jenkinsin alustamista ei mainittu, että uudemmalla kuin JDK 11:ta ei kannata kehittää jos haluaa out-of-the-box Jenkins-tuen, ja koska ongelma on todennäköisesti huomattu vasta kun Jenkins on jo konfiguroitu loppuun asti, hyviä käytänteitä voidaan tässä vaiheessa oikeutetusti sivuuttaa.

## Ohje

> **Huom:** Ohjeessa oletetaan, että käyttäjällä on oletuksena oikeudet ajaa Docker-komentoja. Komentoihin voi olla tarpeen lisätä `sudo`, mikäli näin ei ole.

Paikallista ensin Jenkins-kontin nimi. Sen voi löytää komennolla `docker ps` ja etsimällä kontin nimen `NAMES`-sarakkeesta. Vaihtoehtoisesti voidaan myös käyttää kontin ID:tä (`CONTAINER ID`).

![Kontit](images/containers.png)

Siirry sitten Jenkins-kontin sisään komennolla `docker exec -it <kontin_nimi> /bin/bash`. Komentokehotteen tulisi nyt muuttua kutakuinkin seuraavan näköiseksi.

![Exec](images/docker-exec.png)

Suurin osa kansioista Jenkins-kontissa on (tarkoituksellisesti) lukittuja, eikä niihin voi hankkia kirjoitusoikeuksia. Poikkeuksena tähän on kuitenkin `/var/jenkins_home`, johon voi vapaasti kirjoittaa tiedostoja. Siirtykäämme siis sinne komennolla `cd /var/jenkins_home`.

Etsi uusin versio repositoriosta https://github.com/adoptium/temurin17-binaries/releases (ei mielellään esijulkaisua (pre-release), ellet halua epävakaata koodia). Kirjoitushetkellä viimeisin versio on `jdk17+35` (https://github.com/adoptium/temurin17-binaries/releases/tag/jdk-17%2B35).

Paikallista tästä julkaisusta `OpenJDK17-jdk_x64_linux_hotspot_<versio>.tar.gz`-arkisto ja kopioi sen osoite oikeaklikkaamalla tiedoston nimeä. Kirjoitushetkellä tämä on https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17%2B35/OpenJDK17-jdk_x64_linux_hotspot_17_35.tar.gz. Jätä osoite leikepöydälle.

![Arkisto](images/archive.png)

> **Hox:** `x64` on prosessoriarkkitehtuurimerkintä. Metropolian Educloud-virtuaalipalvelimet käyttävät tätä prosessoriarkkitehtuuria, mutta jos käytät jotain toista ajoympäristöä, saatat tarvita jollekin toiselle prosessoriarkkitehtuurille kootun version (esim. ARM). Voit tarkistaa prosessoriarkkitehtuurisi komennolla `uname -m`.
>
> **Hox 2:** Varo ottamasta vahingossa `alpine_linux`-versiota. Nimi on lähes sama, mutta tulos saattaa olla eri (ja tämän version toimivuutta ei tämän ohjeen teossa ole varmistettu).

Aja sitten seuraavat komennot:

```bash
curl -Lo jdk.tar.gz <arkiston_osoite>
tar -xf jdk.tar.gz
```

Kirjoitushetkellä uusimman version kohdalla komentosarja olisi seuraavanlainen:

```bash
curl -Lo jdk.tar.gz https://github.com/adoptium/temurin17-binaries/releases/download/jdk-17%2B35/OpenJDK17-jdk_x64_linux_hotspot_17_35.tar.gz
tar -xf jdk.tar.gz
```

Jenkinsin kotikansioon pitäisi tämän jälkeen olla ilmestynyt kansio joka kantaa samaa nimeä kuin lataamasi JDK-versio. Pane osoite ns. korvan taakse (muoto on aina `/var/jenkins_home/<versio>`).

![Kansiorakenne](images/folder.png)

Mene tämän jälkeen Jenkinsin pääsivulta Manage Jenkins => Global Tool Configuration => JDK. Klikkaa "Add JDK", ota ruksi pois ruudusta "Install automatically", kirjoita sopiva version nimi, ja liitä sitten edellämainittu polku "JAVA_HOME"-kenttään. Klikkaa tämän jälkeen sivun alareunasta "Save".

![Jenkins](images/jenkins.png)

**Huom:** Jos Jenkins herjaa `/var/jenkins_home/jdk-17+35 is not a directory on the Jenkins controller (but perhaps it exists on some agents)` "JAVA_HOME"-kentän alla, polussasi on kirjoitusvirhe. Tarkista tällöin polun kirjoitusasu.

Mene nyt takaisin koontinäkymään ja yritä ajaa uusi koonti. Koonnin pitäisi nyt mennä läpi.

> Jos tämä ohje ei johda toimivaan koontiin, ja olet seurannut ohjetta pilkulleen, ole hyvä ja ota minuun yhteyttä korjauksia varten Discordissa (LW#0005) tai tule vetämään minua hihasta.
